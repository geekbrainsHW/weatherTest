package weather_app.geekbrains.zero.a2l3;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class MainFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {
    private static final String POSITIVE_BUTTON_TEXT = "Go";
    public static final String SAVE_CITY = "saveCity";
    static final int GALLERY_REQUEST = 1;
    public static final String CITY_WEATHER = "Moscow";
    public static final String TYPE = "text/plain";
    public final static String BROADCAST_ACTION = "weather_app.geekbrains.zero.a2l3.servicebackbroadcast";
    private static final String TAG = "MainFragment";

    BroadcastReceiver broadcastReceiver;
    private ImageView weatherIcon;
    private TextView cityView;
    private TextView currentTempView;
    private TextView detailsView;
    private TextView updateView;
    private DrawerLayout drawer;
    private SharedPreferences sPref;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private ColorStateList colorStateList;


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        weatherIcon = view.findViewById(R.id.weather_icon);
        cityView = view.findViewById(R.id.city_view);
        currentTempView = view.findViewById(R.id.current_temp);
        detailsView = view.findViewById(R.id.details_view);
        updateView = view.findViewById(R.id.update_view);

        if (savedInstanceState != null) {
            onStartService(savedInstanceState.getString(SAVE_CITY));
        }
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String weather = intent.getStringExtra(CITY_WEATHER);
                Log.d(TAG, weather);
                System.out.println(weather);
                updateWeatherData(weather);
            }
        };
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        Objects.requireNonNull(getActivity()).registerReceiver(broadcastReceiver, intentFilter);

        colorSL();
        initFab(view);
        initToolBarAndDrawer(view);
        setHasOptionsMenu(true);
        load();
        return view;
    }


    private void initToolBarAndDrawer(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);

        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(View -> {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType(TYPE);
            intent.putExtra(Intent.EXTRA_TEXT, cityView.getText().toString() + " \n");
            intent.putExtra(Intent.EXTRA_TEXT, currentTempView.getText().toString());
            startActivity(intent);
        });

        drawer = view.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this.getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = view.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (navigationView.getHeaderCount() > 0) {
            View header = navigationView.getHeaderView(0);
            ImageButton selectAvatar = header.findViewById(R.id.user_avatar);

            selectAvatar.setOnClickListener(v -> {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            });
            Picasso.with(getActivity())
                    .load(R.drawable.icon_50n)
                    .into(selectAvatar);
        }


        navigationView.setItemIconTintList(colorStateList);
        navigationView.setItemTextColor(colorStateList);

    }

    private void colorSL() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_selected}, // enabled
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{-android.R.attr.state_pressed}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[]{
                Color.BLACK,
                Color.RED,
                Color.GREEN,
                Color.BLUE
        };

        colorStateList = new ColorStateList(states, colors);
    }

    private void initFab(View view) {
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(view1 -> {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType(TYPE);
            intent.putExtra(Intent.EXTRA_TEXT, cityView.getText().toString() + " \n");
            intent.putExtra(Intent.EXTRA_TEXT, currentTempView.getText().toString());
            startActivity(intent);
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_camera:

                break;
            case R.id.nav_gallery:
                Toast.makeText(getActivity(), "nav_gallery", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_slideshow:
                Toast.makeText(getActivity(), "nav_slideshow", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_manage:
                Toast.makeText(getActivity(), "nav_manage", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_share:
                Toast.makeText(getActivity(), "av_share", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_send:
                Toast.makeText(getActivity(), "nav_send", Toast.LENGTH_LONG).show();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.weather, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.change_city) {
            showInputDialog();
            return true;
        }
        return false;
    }

    private void renderWeather(Weather weather) {
        String city = weather.getCity();
        String temp = weather.getTemp();
        String pressure = weather.getPressure();
        String wind = weather.getWind();
        cityView.setText(city);
        updateView.setText(wind);
        detailsView.setText(pressure);
        currentTempView.setText(temp);
        setWeatherIcon(weather.getIcon());
    }

    private void updateWeatherData(final String city) {
        Log.d(TAG, city);
        final Weather weather = new Gson().fromJson(city, Weather.class);
        renderWeather(weather);
    }

    private void setWeatherIcon(String iconCode) {
        Drawable drawable;
        switch (iconCode) {
            case "01d":
                drawable = getResources().getDrawable(R.drawable.icon_01d);
                break;
            case "01n":
                drawable = getResources().getDrawable(R.drawable.icon_01n);
                break;
            case "02d":
                drawable = getResources().getDrawable(R.drawable.icon_02d);
                break;
            case "02n":
                drawable = getResources().getDrawable(R.drawable.icon_02n);
                break;
            case "03d":
                drawable = getResources().getDrawable(R.drawable.icon_03d);
                break;
            case "03n":
                drawable = getResources().getDrawable(R.drawable.icon_03n);
                break;
            case "04d":
                drawable = getResources().getDrawable(R.drawable.icon_04d);
                break;
            case "04n":
                drawable = getResources().getDrawable(R.drawable.icon_04n);
                break;
            case "09d":
                drawable = getResources().getDrawable(R.drawable.icon_09d);
                break;
            case "09n":
                drawable = getResources().getDrawable(R.drawable.icon_09n);
                break;
            case "10d":
                drawable = getResources().getDrawable(R.drawable.icon_10d);
                break;
            case "10n":
                drawable = getResources().getDrawable(R.drawable.icon_10n);
                break;
            case "11d":
                drawable = getResources().getDrawable(R.drawable.icon_11d);
                break;
            case "11n":
                drawable = getResources().getDrawable(R.drawable.icon_11n);
                break;
            case "13d":
                drawable = getResources().getDrawable(R.drawable.icon_13d);
                break;
            case "13n":
                drawable = getResources().getDrawable(R.drawable.icon_13n);
                break;
            case "50d":
                drawable = getResources().getDrawable(R.drawable.icon_50d);
                break;
            case "50n":
                drawable = getResources().getDrawable(R.drawable.icon_50n);
                break;
            default:
                drawable = getResources().getDrawable(R.drawable.icon_01d);
                break;
        }
        weatherIcon.setImageDrawable(drawable);
    }

    private void showInputDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setTitle(getString(R.string.change_city_dialog));
        final EditText input = new EditText(this.getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(POSITIVE_BUTTON_TEXT, (dialog, which) -> changeCity(input.getText().toString()));
        builder.show();
    }

    public void changeCity(String city) {
        onStartService(city);
    }


    void save() {
        sPref = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(SAVE_CITY, cityView.getText().toString());
        editor.apply();
    }

    void load() {
        sPref = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
        String city = sPref.getString(SAVE_CITY, "null");
        cityView.setText(city);
        onStartService(city);

    }

    @Override
    public void onPause() {
        save();
        onStopService();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putString(SAVE_CITY, cityView.getText().toString());
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = null;
        ImageButton imageButton = Objects.requireNonNull(getActivity()).findViewById(R.id.user_avatar);

        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageButton.setImageBitmap(bitmap);
                }
        }
    }

    @Override
    public void onDestroy() {
        Objects.requireNonNull(getActivity()).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    public void onStartService(String city) {
        Log.d(TAG, city);
        Intent intent = new Intent(getContext(), WeatherService.class);
        intent.putExtra(CITY_WEATHER, city);
        Objects.requireNonNull(getActivity()).startService(intent);
    }

    public void onStopService() {
        Intent intent = new Intent(getContext(), WeatherService.class);
        Objects.requireNonNull(getActivity()).stopService(intent);
    }
}

