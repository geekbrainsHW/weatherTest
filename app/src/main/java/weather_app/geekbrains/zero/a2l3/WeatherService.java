package weather_app.geekbrains.zero.a2l3;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Objects;

public class WeatherService extends IntentService {
    private static final String TAG = "StartedService";
    private final Handler handler = new Handler();

    public WeatherService() {
        super("weatherService");
    }

    @Override
    protected void onHandleIntent(@Nullable final Intent intent) {
        Bundle bundle = Objects.requireNonNull(intent).getExtras();
        String city = "";
        if (bundle != null) {
            Log.d(TAG, bundle.getString(MainFragment.CITY_WEATHER));
            city = bundle.getString(MainFragment.CITY_WEATHER, "fail");
        }

        final JSONObject json = WeatherDataLoader.getJSONData(city);
        if (json == null) {
            handler.post(() -> Toast.makeText(getApplicationContext(), getString(R.string.place_not_found),
                    Toast.LENGTH_LONG).show());
        } else {
//            final Weather weather = new Gson().fromJson(json.toString(), Weather.class);
            final String finalCity = json.toString();
            handler.post(() -> {
                Intent broadcastIntent = new Intent(MainFragment.BROADCAST_ACTION);
                broadcastIntent.putExtra(MainFragment.CITY_WEATHER, finalCity);
                Log.i(TAG, finalCity);
                sendBroadcast(broadcastIntent);
            });
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }
}
